package com.test.notification.model;

/**
 * Jude.Mgbeji created on 28/06/2020
 **/

public class Response {
    private String message;
    private Status status;
    private String code;

    public Response(String message, Status status, String code) {
        this.message = message;
        this.status = status;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
