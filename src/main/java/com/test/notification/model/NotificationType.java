package com.test.notification.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.List;

public enum NotificationType {
    EMAIL("Email"),
    SMS("sms");
    private String description;

    NotificationType(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static List<NotificationType> getNotificationType(){
        return Arrays.asList(NotificationType.values());
    }

}
