package com.test.notification.model;

public enum Status {
    SENT("Sent"),
    PENDING("Pending"),
    FAILED("Failed");
    private String description;

    Status(String description) {
        this.description = description;
    }
}
