package com.test.notification.model;

/**
 * Jude.Mgbeji created on 28/06/2020
 **/

public class Request {
    private String recipientEmail;
    private String subject;
    private String message;
    private String senderPhoneNumber;
    private String recieverPhoneNumber;
    private  NotificationType notificationType;

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    public void setSenderPhoneNumber(String senderPhoneNumber) {
        this.senderPhoneNumber = senderPhoneNumber;
    }

    public String getRecieverPhoneNumber() {
        return recieverPhoneNumber;
    }

    public void setRecieverPhoneNumber(String recieverPhoneNumber) {
        this.recieverPhoneNumber = recieverPhoneNumber;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
