package com.test.notification.services.implementations;

import com.test.notification.model.NotificationType;
import com.test.notification.model.Request;
import com.test.notification.model.Response;
import com.test.notification.model.Status;
import com.test.notification.services.abstractions.NotificationProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 * Jude.Mgbeji created on 27/06/2020
 **/
@Service
public class MailService implements NotificationProcessor {

    private JavaMailSender javaMailSender;

    @Value("${mail.from}")
    private String sender;

    @Autowired
    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public Response send(Request mail) {
try {
    MimeMessagePreparator messagePreparator = mimeMessage -> {
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
        messageHelper.setFrom(sender);
        messageHelper.setTo(mail.getRecipientEmail());
        messageHelper.setSubject(mail.getSubject());
        messageHelper.setText(mail.getMessage());
    };
    javaMailSender.send(messagePreparator);
    return new Response("Mail sent successfully", Status.SENT,"");
}catch (Exception e){
    return new Response("Cannot send mail at this time, kindly contact your administrator", Status.FAILED,"");
}

    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.EMAIL;
    }

}
