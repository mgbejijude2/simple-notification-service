package com.test.notification.services.implementations;

import com.test.notification.model.*;
import com.test.notification.services.abstractions.NotificationProcessor;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Jude.Mgbeji created on 27/06/2020
 **/

@Service
public class SmsService implements NotificationProcessor {

    @Value("${sms.auth.token}")
    private String token;

    @Value("${sms.account.id}")
    private String accountId;

    @Override
    public Response send(Request sms) {
        Twilio.init(accountId, token);
try {
    Message message = Message
            .creator(new PhoneNumber(sms.getRecieverPhoneNumber()), // to
                    new PhoneNumber("+14758828952"), // from
                    sms.getMessage())
            .create();

    System.out.println(message.getSid());
    return new Response("SMS sent successfully", Status.SENT,"");
}catch (Exception e){
    return new Response("Cannot send sms at this time, kindly contact your administrator", Status.FAILED,"");
}

    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.SMS;
    }


}
