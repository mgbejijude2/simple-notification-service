package com.test.notification.services.abstractions;

import com.test.notification.model.NotificationType;
import com.test.notification.model.Request;
import com.test.notification.model.Response;

/**
 * Jude.Mgbeji created on 27/06/2020
 **/

public interface NotificationProcessor {
    Response send(Request request);
    NotificationType getNotificationType();
}
