package com.test.notification.services;

import com.test.notification.model.NotificationType;
import com.test.notification.services.abstractions.NotificationProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.InvalidKeyException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Jude.Mgbeji created on 27/06/2020
 **/

@Component
public class NotificationServiceFactory {

    private Map<NotificationType, NotificationProcessor> processors = new ConcurrentHashMap<>();

    @Autowired
    public NotificationServiceFactory(List<NotificationProcessor> processors) {
        for (NotificationProcessor processor:
             processors) {
            if(this.processors.containsKey(processor.getNotificationType())){
                throw new IllegalStateException("A Notification type can only be mapped to a single processor");
            }
            this.processors.put(processor.getNotificationType(),processor);
        }
    }

    public NotificationProcessor getProcessor(NotificationType type) throws InvalidKeyException {
        NotificationProcessor processor= processors.get(type);
        if (null == processor){
            throw new InvalidKeyException("No processor defined for type "+type);
        }
        return processor;
    }
}
