package com.test.notification.controllers;

import com.test.notification.model.Request;
import com.test.notification.model.Response;
import com.test.notification.services.NotificationServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.InvalidKeyException;

/**
 * Jude.Mgbeji created on 27/06/2020
 **/
@RestController
@RequestMapping("/notification")
public class NotificationController {

    @Autowired
    private NotificationServiceFactory noticationServiceFactory;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> notify(Request request
                       ) throws InvalidKeyException {
              Response response=  noticationServiceFactory.getProcessor(request.getNotificationType()).send(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}

